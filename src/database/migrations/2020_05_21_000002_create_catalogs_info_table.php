<?php

use VmdCms\CoreCms\CoreModules\Languages\Models\CoreLanguage;
use VmdCms\Modules\Catalogs\Models\CatalogInfo as model;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogsInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(model::table(), function (Blueprint $table){
            $table->increments('id');
            $table->string(model::LANG_KEY,5)->nullable();
            $table->integer(model::getForeignField())->unsigned();
            $table->string('url',150)->nullable();
            $table->string('title',150)->nullable();
            $table->string('title_short',150)->nullable();
            $table->text('description_short')->nullable();
            $table->longText('description')->nullable();
            $table->longText('data')->nullable();
            $table->timestamps();
            $table->unique([model::LANG_KEY, model::getForeignField()],'cat_inf_cat_id_idx');
            $table->unique([model::LANG_KEY, 'url'],'cat_inf_lang_idx');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::getForeignField(), 'cat_inf_cat_id_fk')
                ->references(model::getBaseModelClass()::getPrimaryField())->on(model::getBaseModelClass()::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });

        Schema::table(model::table(), function (Blueprint $table){
            $table->foreign(model::LANG_KEY, 'cat_inf_lang_fk')
                ->references(CoreLanguage::getPrimaryField())->on(CoreLanguage::table())
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(model::table());
    }
}
