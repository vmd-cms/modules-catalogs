<?php

use VmdCms\Modules\Catalogs\Models\Catalog as FirstModel;
use VmdCms\Modules\Taxonomies\Models\Taxonomy as SecondModel;
use VmdCms\Modules\Catalogs\Models\CatalogTaxonomy as CrossModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogsTaxonomiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!class_exists(\App\Modules\Taxonomies\Models\Taxonomy::class)){
            return;
        }

        Schema::create(CrossModel::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer(FirstModel::table() . '_id')->unsigned();
            $table->integer(SecondModel::table() . '_id')->unsigned();
            $table->timestamps();
            $table->unique([FirstModel::table() . '_id', SecondModel::table() . '_id']);
        });
        Schema::table(CrossModel::table(), function (Blueprint $table){
            $table->foreign(FirstModel::table() . '_id', CrossModel::table() . '_' . FirstModel::table() . '_id' .'_fk')
                ->references(FirstModel::getPrimaryField())->on(FirstModel::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(SecondModel::table() . '_id', CrossModel::table() . '_' . SecondModel::table() . '_id' .'_fk')
                ->references(SecondModel::getPrimaryField())->on(SecondModel::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CrossModel::table());
    }
}
