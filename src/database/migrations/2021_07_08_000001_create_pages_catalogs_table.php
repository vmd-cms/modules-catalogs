<?php

use VmdCms\Modules\Catalogs\Models\Catalog as FirstModel;
use VmdCms\Modules\Catalogs\Models\PageCatalog as CrossModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!class_exists(\VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::class)){
            return;
        }
        Schema::create(CrossModel::table(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer(FirstModel::table() . '_id')->unsigned();
            $table->integer(\VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::table() . '_id')->unsigned();
            $table->timestamps();
            $table->unique([FirstModel::table() . '_id', \VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::table() . '_id']);
        });
        Schema::table(CrossModel::table(), function (Blueprint $table){
            $table->foreign(FirstModel::table() . '_id', CrossModel::table() . '_' . FirstModel::table() . '_id' .'_fk')
                ->references(FirstModel::getPrimaryField())->on(FirstModel::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(\VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::table() . '_id', CrossModel::table() . '_' . \VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::table() . '_id' .'_fk')
                ->references(\VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::getPrimaryField())->on(\VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::table())
                ->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CrossModel::table());
    }
}
