<?php
namespace VmdCms\Modules\Catalogs\database\seeds;

use Illuminate\Support\Str;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Initializers\AbstractModuleSeeder;
use App\Modules\Catalogs\Sections\Catalog;
use App\Modules\Catalogs\Sections\Components\CatalogGroupBlock;
use App\Modules\Catalogs\Sections\Components\CatalogTranslate;
use VmdCms\Modules\Catalogs\Models\CatalogInfo;

class ModuleSeeder extends AbstractModuleSeeder
{
    /**
     * @inheritDoc
     */
    protected function getSectionsAssoc(): array
    {
        return [
            Catalog::class => CoreLang::get('catalog',$this->moduleInitializer::moduleSlug()),
            CatalogGroupBlock::class => CoreLang::get('info_blocks') . ' ' . CoreLang::get('catalog',$this->moduleInitializer::moduleSlug()),
            CatalogTranslate::class => CoreLang::get('translates') . ' ' . CoreLang::get('catalog',$this->moduleInitializer::moduleSlug()),
        ];
    }

    /**
     * @inheritDoc
     */
    protected function getNavigationAssoc(): array
    {
        return [
            [
                "slug" => "shop_group",
                "title" => CoreLang::get('shop'),
                "is_group_title" => true,
                "order" => 2,
                "children" => [
                    [
                        "slug" => "catalogs",
                        "title" => CoreLang::get('catalog'),
                        "section_class" => Catalog::class,
                        "icon" => "icon icon-shop",
                    ]
                ]
            ],
            [
                "slug" => "content_group",
                "title" => CoreLang::get('content'),
                "is_group_title" => true,
                "order" => 3,
                "children" => [
                    [
                        "slug" => "block_groups",
                        "title" => CoreLang::get('info_blocks'),
                        "icon" => "icon icon-copy",
                        "children" => [
                            [
                                "slug" => "catalog_blocks",
                                "title" => CoreLang::get('catalog',$this->moduleInitializer::moduleSlug()),
                                "section_class" => CatalogGroupBlock::class,
                                "order" => 5,
                            ]
                        ]
                    ]
                ]
            ],
            [
                "slug" => "configs",
                "title" => CoreLang::get('configurations'),
                "is_group_title" => true,
                "order" => 5,
                "children" => [
                    [
                        "slug" => "translate_groups",
                        "title" => CoreLang::get('translates'),
                        "order" => 2,
                        "icon" => "icon icon-card",
                        "children" => [
                            [
                                "slug" => "catalog_translates",
                                "title" => CoreLang::get('catalog',$this->moduleInitializer::moduleSlug()),
                                "section_class" => CatalogTranslate::class,
                                "order" => 6
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function seedModelData()
    {
        try {
            $catalogArrPath = base_path('database/seeds/modules/catalogs_arr.php');
            if(!file_exists($catalogArrPath)) throw new \Exception();
            $arr = include $catalogArrPath;
            if(!is_array($arr) || !count($arr)) throw new \Exception();
            foreach ($arr as $item) {
                $this->recursiveSeed($item, null);
            }
        }
        catch (\Exception $exception){

        }
    }

    protected function recursiveSeed($item, $parent_id)
    {
        $model = new \VmdCms\Modules\Catalogs\Models\Catalog();
        $model->parent_id = $parent_id;
        $model->key = $item['key'] ?? null;
        $model->url = $item['url'] ?? Str::slug($item['title']);
        $model->active = $item['active'];
        $model->save();
        $modelInfo = new CatalogInfo();
        $modelInfo->catalogs_id = $model->id;
        $modelInfo->title = $item['title'];
        $modelInfo->url = $item['url'] ?? Str::slug($item['title']);
        $modelInfo->save();
        if(isset($item['children']) && count($item['children']))
        {
            foreach ($item['children'] as $child) $this->recursiveSeed($child, $model->id);
        }
        return;
    }

}
