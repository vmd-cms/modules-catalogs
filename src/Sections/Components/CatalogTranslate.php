<?php

namespace VmdCms\Modules\Catalogs\Sections\Components;

use VmdCms\CoreCms\CoreModules\Translates\Sections\AbstractTranslate;

class CatalogTranslate extends AbstractTranslate
{
    /**
     * @var string
     */
    protected $slug = 'catalog_translates';

    public function getCmsModelClass(): string
    {
        return \App\Modules\Catalogs\Models\Components\CatalogTranslate::class;
    }
}
