<?php

namespace VmdCms\Modules\Catalogs\Sections\Components;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Contracts\Sections\AdminSectionInterface;
use VmdCms\CoreCms\CoreModules\Content\Sections\Blocks\Block;
use VmdCms\CoreCms\Exceptions\Sections\SectionCmsModelException;
use VmdCms\CoreCms\Exceptions\Sections\SectionNotFoundException;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Services\CoreRouter;
use App\Modules\Catalogs\Services\BlockEnum;
use App\Modules\Catalogs\Services\BlockSectionFactory;

class CatalogGroupBlock extends Block
{
    /**
     * @var string
     */
    protected $slug = 'catalog_blocks';

    /**
     * @return string
     */
    protected function getBaseCmsModelClass()
    {
        return \App\Modules\Catalogs\Models\Components\CatalogGroupBlock::class;
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return Form::panel([
            FormComponent::select('key')->setEnumValues(BlockEnum::getEnums())->unique()->required(),
            FormComponent::switch('active'),
            FormComponent::input('description')
        ]);
    }

    protected $inheritSection;

    /**
     * @return AdminSectionInterface
     * @throws SectionCmsModelException
     * @throws SectionNotFoundException
     */
    protected function getInheritSection()
    {
        if(Route::current()->getName() != CoreRouter::ROUTE_EDIT_GET &&
            Route::current()->getName() != CoreRouter::ROUTE_EDIT_PUT &&
            empty(request()->id))
        {
            throw new SectionCmsModelException();
        }

        if(!$this->inheritSection instanceof AdminSectionInterface)
        {
            $baseModelClass = $this->getBaseCmsModelClass();
            $model = $baseModelClass::find(request()->id);
            if(!$model instanceof \VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block) throw new ModelNotFoundException();
            $this->inheritSection =  (new BlockSectionFactory())->getBlockModel($model->key);
        }
        return $this->inheritSection;
    }
}
