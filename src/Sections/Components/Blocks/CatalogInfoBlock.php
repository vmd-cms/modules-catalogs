<?php

namespace VmdCms\Modules\Catalogs\Sections\Components\Blocks;

use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\Modules\Catalogs\Sections\Components\CatalogGroupBlock;

class CatalogInfoBlock extends CatalogGroupBlock
{
    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('key')->setDisabled(true),
            FormComponent::switch('active'),
            FormComponent::input('description'),
            FormComponent::input('title'),
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Catalogs\Models\Components\Blocks\CatalogInfoBlock::class;
    }

}

