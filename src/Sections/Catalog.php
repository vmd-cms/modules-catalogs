<?php

namespace VmdCms\Modules\Catalogs\Sections;

use VmdCms\CoreCms\Contracts\Dashboard\Display\DisplayInterface;
use VmdCms\CoreCms\Contracts\Dashboard\Forms\FormInterface;
use VmdCms\CoreCms\CoreModules\CoreTranslates\Services\CoreLang;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterDisplay;
use VmdCms\CoreCms\Dashboard\Display\Filters\FilterTreeComponent;
use VmdCms\CoreCms\Facades\Column;
use VmdCms\CoreCms\Facades\ColumnEditable;
use VmdCms\CoreCms\Facades\Display;
use VmdCms\CoreCms\Facades\Form;
use VmdCms\CoreCms\Facades\FormComponent;
use VmdCms\CoreCms\Models\CmsSection;
use VmdCms\Modules\Catalogs\Initializers\ModuleInitializer;

class Catalog extends CmsSection
{
    /**
     * @var string
     */
    protected $slug = 'catalogs';

    /**
     * @inheritDoc
     */
    public function getTitle() : string
    {
        return CoreLang::get('catalog');
    }

    public function display()
    {
        $filter = (new FilterDisplay())
            ->appendFilterComponent((new FilterTreeComponent('info.title'))
                ->setHeadTitle($this->getTitle())
                ->setModelClass($this->getCmsModelClass())
                ->setFilterSlug('parent_id')
                ->setFilterField('parent_id')
                ->setMaxLevel(1)
            );
        return Display::dataTable([
            ColumnEditable::text('info.title',CoreLang::get('title'))
                ->setSearchableCallback(function ($query, $search) {
                    $query->orWhereHas('info', function ($q) use ($search) {
                        $q->where('title', 'like', '%' . $search . '%');
                    });
                })
                ->setSortableCallback(function ($query, $sortType) {
                    $query->join('catalogs_info', function ($join) {
                        $join->on('catalogs.id', '=', 'catalogs_info.catalogs_id');
                    });
                    $query->orderBy('catalogs_info.title', $sortType)
                        ->selectRaw('catalogs.*');
                }),
            ColumnEditable::text('url','URL')->searchable()->sortable()->unique(),
            ColumnEditable::switch('active')->sortable()->alignCenter(),
        ])->orderDefault(function ($query){
            $query->orderBy('created_at','desc');
        })
            ->whereNull('parent_id')
            ->setSearchable(true)
            ->setFilter($filter)->orderable();
    }

    /**
     * @return FormInterface
     */
    public function create() : FormInterface
    {
        return $this->edit(null);
    }

    /**
     * @param int|null $id
     * @return FormInterface
     */
    public function edit(?int $id) : FormInterface
    {
        return Form::panel([
            FormComponent::input('info.title',CoreLang::get('title'))->required(),
            FormComponent::url('url','Url')->setDependedField('info.title')->required()->unique(),
            FormComponent::radio('active'),
            FormComponent::select('parent_id',CoreLang::get('parent_catalog',ModuleInitializer::moduleSlug()))
                ->setModelForOptions($this->getCmsModelClass())
                ->setDisplayField('info.title')
                ->setForeignField('id')
                ->setWhere(['parent_id','=',null])
                ->setWhere(['id','!=',$id])
                ->nullable(),
            FormComponent::checkbox('filters',CoreLang::get('filters',ModuleInitializer::moduleSlug()))
                ->setWhere(['parent_id','=',null])
                ->setDisplayField('info.title'),
            FormComponent::ckeditor('info.description',CoreLang::get('description')),
            FormComponent::image('photo',CoreLang::get('image')),
            FormComponent::seo()
        ]);
    }

    public function getCmsModelClass(): string
    {
        return \App\Modules\Catalogs\Models\Catalog::class;
    }
}
