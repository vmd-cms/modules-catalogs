<?php

namespace VmdCms\Modules\Catalogs\Contracts;

use VmdCms\Modules\Catalogs\Models\Catalog;

interface CatalogDTOInterface
{
    /**
     * CatalogEntityInterface constructor.
     * @param Catalog $catalog
     */
    public function __construct(Catalog $catalog);

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @return string|null
     */
    public function getImagePath(): ?string;

    /**
     * @return string|null
     */
    public function getUrlPath(): ?string;

}
