<?php

namespace VmdCms\Modules\Catalogs\Contracts;

use VmdCms\CoreCms\Contracts\Collections\CollectionInterface;

interface CatalogDTOCollectionInterface extends CollectionInterface
{
    /**
     * @param CatalogDTOInterface $catalogEntity
     */
    public function append(CatalogDTOInterface $catalogEntity);

}
