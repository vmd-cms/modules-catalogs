<?php

namespace VmdCms\Modules\Catalogs\DTO;

use Illuminate\Contracts\Support\Arrayable;
use VmdCms\Modules\Catalogs\Contracts\CatalogDTOInterface;
use VmdCms\Modules\Catalogs\Models\Catalog;

class CatalogDTO implements CatalogDTOInterface, Arrayable
{
    protected $id;
    protected $title;
    protected $imagePath;
    protected $urlPath;

    public function __construct(Catalog $catalog)
    {
        $this->id = $catalog->id ?? null;
        $this->title = $catalog->info->title ?? null;
        $this->imagePath = $catalog->imagePath ?? null;
        $this->urlPath = $catalog->urlPath ?? null;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getImagePath(): ?string
    {
        return $this->imagePath;
    }

    /**
     * @return string|null
     */
    public function getUrlPath(): ?string
    {
        return $this->urlPath;
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'imagePath' => $this->imagePath,
            'urlPath' => $this->urlPath,
        ];
    }
}
