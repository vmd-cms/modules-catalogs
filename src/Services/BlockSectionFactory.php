<?php

namespace VmdCms\Modules\Catalogs\Services;

use VmdCms\CoreCms\Services\BlockSectionFactoryAbstract;
use VmdCms\Modules\Catalogs\Models\Components\Blocks\CatalogInfoBlock;

class BlockSectionFactory extends BlockSectionFactoryAbstract
{
    protected static function getAssocKeySectionClasses() : array
    {
        return [
            CatalogInfoBlock::getModelKey() => \App\Modules\Catalogs\Sections\Components\Blocks\CatalogInfoBlock::class,
        ];
    }
}
