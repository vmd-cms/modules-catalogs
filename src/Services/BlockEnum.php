<?php

namespace VmdCms\Modules\Catalogs\Services;

use VmdCms\CoreCms\Services\Enums;

class BlockEnum extends Enums
{
    const CATALOG_INFO_BLOCK = 'catalog_info_block';
}
