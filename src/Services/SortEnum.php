<?php

namespace VmdCms\Modules\Catalogs\Services;

use VmdCms\CoreCms\Services\Enums;

class SortEnum extends Enums
{
    const PRICE_ASC = 'price_asc';
    const PRICE_DESC = 'price_desc';
}
