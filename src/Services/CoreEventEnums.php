<?php

namespace VmdCms\Modules\Catalogs\Services;

class CoreEventEnums
{
    const USER_CATALOG_VIEW = 'user.catalog.view';
    const USER_CATALOG_SEARCH_ACTION = 'user.catalog_search.action';
}
