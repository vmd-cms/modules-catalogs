<?php

namespace VmdCms\Modules\Catalogs\Entity;

use App\Modules\Catalogs\Models\Catalog;
use App\Modules\Catalogs\Models\Pages\BrandsPage;
use App\Modules\Catalogs\Models\Pages\CatalogPage;
use App\Modules\Catalogs\Models\Pages\TargetsPage;
use App\Modules\Content\DTO\InfoPageDTO;
use App\Modules\Content\Services\DataShare;
use App\Modules\Taxonomies\DTO\TaxonomyDTO;
use App\Modules\Taxonomies\Models\Brand;
use Illuminate\Support\Str;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Services\BreadCrumbs;

class PageEntity
{
    public function shareCatalogsPageData()
    {
        if(!$page = CatalogPage::first()) {
            throw new ModelNotFoundException();
        }

        $dataShare = DataShare::getInstance();

        $dataShare->seo->setData($page);
        $dataShare->appendData('pageDTO',new InfoPageDTO($page));

        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto('',$page->info->title ?? ' '));
        $dataShare->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());
    }

    /**
     * @throws ModelNotFoundException
     */
    public function shareBrandsPageData()
    {
        if(!$page = BrandsPage::first()) throw new ModelNotFoundException();

        $dataShare = DataShare::getInstance();

        $dataShare->seo->setData($page);

        $dataShare->appendData('pageDTO',new InfoPageDTO($page));

        $brands = Brand::active()->whereHas('products')->get();
        $brandsGroupedNumeric = $brandsGroupedEn = $brandsGroupedCyr = [];

        if(is_countable($brands) && count($brands)){
            foreach ($brands as $item){
                $key = mb_substr($item->info->title ?? 0, 0, 1);
                if(intval($key) === 0) {
                    if(preg_match( '/[\p{Cyrillic}]/u', $key) !== 0){
                        $brandsGroupedCyr[Str::upper($key)][] = new TaxonomyDTO($item,false);
                    }
                    else{
                        $brandsGroupedEn[Str::upper($key)][] = new TaxonomyDTO($item,false);
                    }

                }
                else{
                    $brandsGroupedNumeric[intval($key)][] = new TaxonomyDTO($item,false);
                }
            }
        }

        ksort($brandsGroupedNumeric);
        ksort($brandsGroupedEn);
        ksort($brandsGroupedCyr);

        $dataShare->appendData('brandsGroupedCyr',$brandsGroupedCyr);
        $dataShare->appendData('brandsGroupedEn',$brandsGroupedEn);
        $dataShare->appendData('brandsGroupedNumeric',$brandsGroupedNumeric);

        BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto('',$page->info->title ?? ' '));
        $dataShare->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());
    }

}
