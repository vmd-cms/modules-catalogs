<?php

namespace VmdCms\Modules\Catalogs\Entity;

use App\Modules\Catalogs\DTO\CatalogDTO;
use App\Modules\Catalogs\Models\Catalog;
use App\Modules\Catalogs\Models\CatalogInfo;
use App\Modules\Catalogs\Models\Components\CatalogTranslate;
use App\Modules\Catalogs\Services\SortEnum;
use App\Modules\Content\Entity\Languages;
use App\Modules\Content\Services\DataShare;
use App\Modules\Prices\DTO\PriceDTO;
use App\Modules\Prices\Models\Price;
use App\Modules\Products\Entity\ProductCatalogEntity;
use App\Modules\Products\Models\Components\ProductTranslate;
use App\Modules\Taxonomies\Models\Brand;
use App\Modules\Taxonomies\Models\Filter;
use App\Modules\Taxonomies\Models\Status;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use VmdCms\CoreCms\Collections\NavsCollection;
use VmdCms\CoreCms\Contracts\Models\CmsModelInterface;
use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\DTO\Dashboard\BreadCrumbDto;
use VmdCms\CoreCms\DTO\NavDTO;
use VmdCms\CoreCms\Exceptions\Models\ModelNotFoundException;
use VmdCms\CoreCms\Services\BreadCrumbs;
use VmdCms\CoreCms\Services\Files\StorageAdapter;
use VmdCms\CoreCms\Services\Pagination;
use VmdCms\CoreCms\Traits\Navs\PrepareNavsCollection;
use VmdCms\Modules\Prices\Collections\PriceDTOCollection;
use VmdCms\Modules\Taxonomies\Collections\TaxonomyDTOCollection;
use VmdCms\Modules\Taxonomies\Contracts\TaxonomyDTOCollectionInterface;
use VmdCms\Modules\Taxonomies\DTO\TaxonomyDTO;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

class CatalogEntity
{
    use PrepareNavsCollection;

    protected $catalog;

    /**
     * @param string $fullUrl
     * @throws ModelNotFoundException
     */
    public function shareCatalogsPageData(string $fullUrl)
    {
        $this->shareCatalogByUrl($fullUrl);
        $this->shareSubCatalogs();
        $this->shareSeoData();
        $this->shareBreadCrumbs();

        $prices = Price::limit(5)->get();
        $collection = new PriceDTOCollection();
        foreach ($prices as $item)
        {
            $collection->append(new PriceDTO($item));
        }

        DataShare::getInstance()->appendData('priceProductItems',$collection->getItems());
    }

    /**
     * @param string $fullUrl
     * @param int $page
     * @throws ModelNotFoundException
     */
    public function shareCatalogPageData(string $fullUrl, int $page = 1)
    {
        DataShare::getInstance()->translates->appendGroups([ProductTranslate::getModelGroup(),CatalogTranslate::getModelGroup()]);
        $this->shareCatalogByUrl($fullUrl);

        $this->shareCatalogTaxonomies();
        $this->shareSelectedCatalogTaxonomies();
        $this->shareSelectedPriceRange();
        $this->shareSortType();

        $this->shareSubCatalogs();

        $this->shareProductPricesData($page);

        $this->shareSeoData();
        $this->shareBreadCrumbs();
    }

    /**
     * @param int $catalogId
     * @param int $page
     * @throws ModelNotFoundException
     */
    public function shareCatalogPaginationData(int $catalogId, int $page = 1)
    {
        $this->shareCatalogById($catalogId);

        $this->shareCatalogFilters();
        $this->shareSelectedCatalogFilters();

    }

    protected function shareSubCatalogs()
    {
        if($this->catalog instanceof Catalog){
            $items = $this->prepareCollection($this->catalog->childrenTree)->getItems();
            DataShare::getInstance()->appendData('subCatalogs',$items);
        }
    }

    /**
     * @param string $fullUrl
     * @return Catalog|CmsModelInterface|null
     * @throws ModelNotFoundException
     */
    protected function shareCatalogByUrl(string $fullUrl)
    {
        $this->catalog = (new Catalog())->getModelByFullUrl($fullUrl);

        if(!$this->catalog instanceof Catalog) {
            throw new ModelNotFoundException();
        }
        DataShare::getInstance()->appendData('catalog',new \VmdCms\Modules\Catalogs\DTO\CatalogDTO($this->catalog));
        return $this->catalog;
    }

    /**
     * @param int $catalogId
     * @return Catalog
     * @throws ModelNotFoundException
     */
    protected function shareCatalogById(int $catalogId)
    {
        $this->catalog = Catalog::where('id',$catalogId)->childrenTree(true)->first();
        if(!$this->catalog instanceof Catalog) throw new ModelNotFoundException();
        DataShare::getInstance()->appendData('catalog',new CatalogDTO($this->catalog));
        return $this->catalog;
    }

    /**
     * @return array
     */
    protected function getCatalogIdsArrByCatalog()
    {
        return $this->getCatalogIdsArr($this->catalog);
    }

    protected function getCatalogIdsArr(Catalog $catalog, &$catalogIds = [])
    {
        $catalogIds[$catalog->id] = $catalog->id;
        if(!$catalog->childrenTree)
        {
            return $catalogIds;
        }
        foreach ($catalog->childrenTree as $child)
        {
            if(!$child->childrenTree)
            {
                $catalogIds[$child->id] = $child->id;
                continue;
            }
            $this->getCatalogIdsArr($child,$catalogIds);
        }
        return $catalogIds;
    }

    protected function shareSeoData()
    {
        if($this->catalog instanceof SeoableInterface)
        {
            DataShare::getInstance()->seo->setData($this->catalog);
        }
    }

    protected function shareCatalogFilters()
    {
        DataShare::getInstance()->appendData('filters',$this->getTaxonomyDtoCollection($this->catalog->filters ?? null)->getItems());
    }

    /**
     * @param Collection|null $taxonomies
     * @return TaxonomyDTOCollectionInterface
     */
    protected function getTaxonomyDtoCollection(Collection $taxonomies = null): TaxonomyDTOCollectionInterface
    {
        $filtersCollection = new TaxonomyDTOCollection();
        if(is_countable($taxonomies) && count($taxonomies)){
            foreach ($taxonomies as $item)
            {
                if(!$item instanceof Taxonomy) continue;
                $filtersCollection->append(new TaxonomyDTO($item));
            }
        }
        return $filtersCollection;
    }

    protected function shareSelectedCatalogFilters()
    {
        $selectedFiltersCollection = new TaxonomyDTOCollection();
        $selectedIdsArr = [];
        $catalogFilters = DataShare::getInstance()->filters ?? null;

        if(!is_countable($catalogFilters) || !count($catalogFilters)) {
            return;
        }

        foreach ($catalogFilters as $filterDto)
        {
            if($filterDto->getUrl() && $queryStr = request()->get($filterDto->getUrl()))
            {
                $childrenCollection = new TaxonomyDTOCollection();
                $queryArr = explode(',',$queryStr);
                foreach ($filterDto->getChildren()->getItems() as $subItem)
                {
                    if(array_search($subItem->getUrl(), $queryArr) !== false){
                        $childrenCollection->append($subItem);
                        $selectedIdsArr[$subItem->getId()] = $subItem->getId();
                    }
                }
                $selectedFilterDto = clone $filterDto;
                $selectedFilterDto->setChildren($childrenCollection);
                $selectedFiltersCollection->append($selectedFilterDto);
            }
        }

        DataShare::getInstance()->appendData('filtersSelectedIds',$selectedIdsArr);
        DataShare::getInstance()->appendData('filtersSelected',$selectedFiltersCollection->getItems());
    }

    protected function shareBreadCrumbs()
    {
        $this->setBreadCrumbs($this->catalog);
        DataShare::getInstance()->appendData('breadCrumbs',BreadCrumbs::getInstance()->getItems());
    }

    public function setBreadCrumbs(\VmdCms\Modules\Catalogs\Models\Catalog $catalog = null)
    {
        if($catalog instanceof \VmdCms\Modules\Catalogs\Models\Catalog ){
            BreadCrumbs::getInstance()->prependItem(new BreadCrumbDto($catalog->urlPath,$catalog->info->title ?? ''));

            if($catalog->parent instanceof CmsModelInterface) {
                $this->setBreadCrumbs($catalog->parent);
            }
        }
    }


    /**
     * @param Collection|null $filters
     */
    public function shareTaxonomies()
    {
        $taxonomies = new Collection();

        $items = Status::active()->order()->get();
        $statusDto = new \App\Modules\Taxonomies\DTO\TaxonomyDTO();
        $statusDto->url = Status::getAnchorKey();
        $statusDto->setChildren($this->getTaxonomyDtoCollection($items));
        $taxonomies->put('statuses',collect([$statusDto]));

        $items = Brand::active()->order()->get();
        $brandsDto = new \App\Modules\Taxonomies\DTO\TaxonomyDTO();
        $brandsDto->url = Brand::getAnchorKey();
        $brandsDto->setChildren($this->getTaxonomyDtoCollection($items));
        $taxonomies->put('brands',collect([$brandsDto]));

        $filters = isset($this->catalog->filters) ? $this->catalog->filters :  new Collection();
        if($needle = request()->get('needle')){
            $searchEntity = new SearchEntity($needle);
            $taxonomyIds = $searchEntity->getTaxonomyIdsByNeedle();
            $needleFilters = Filter::whereIn('id',$taxonomyIds)->with('info','parent')->get();
            $filters = $filters->merge($needleFilters);
        }

        $taxonomies->put('filters',$this->getTaxonomyDtoCollection($filters)->getItems());

        DataShare::getInstance()->appendData('taxonomies',$taxonomies);
    }

    public function shareSelectedCatalogTaxonomies()
    {
        $selectedGroupedIdsArr = [];
        $catalogTaxonomiesGroups = DataShare::getInstance()->taxonomies ?? null;

        if(!is_countable($catalogTaxonomiesGroups) || !count($catalogTaxonomiesGroups)) {
            return;
        }

        foreach ($catalogTaxonomiesGroups as $group)
        {
            foreach ($group as $taxonomyDto)
            {
                if($taxonomyDto->getUrl() && $queryStr = request()->get($taxonomyDto->getUrl()))
                {
                    $queryArr = explode(',',$queryStr);
                    foreach ($taxonomyDto->getChildren()->getItems() as $subItem)
                    {
                        if(array_search($subItem->getUrl(), $queryArr) !== false){
                            $selectedGroupedIdsArr[$taxonomyDto->getUrl()][$subItem->getId()] = $subItem->getId();
                        }
                    }
                }
            }
        }
        DataShare::getInstance()->appendData('selectedTaxonomyGrouped',$selectedGroupedIdsArr);
    }

    protected function shareSelectedPriceRange()
    {
        if($minPrice = request()->get('min-price')){
            DataShare::getInstance()->appendData('minPrice',$minPrice);
        }

        if($maxPrice = request()->get('max-price')){
            DataShare::getInstance()->appendData('maxPrice',$maxPrice);
        }
    }


    protected function shareProductPricesData(int $page = 1)
    {
        $dataShare = DataShare::getInstance();

        $catalogIds = $this->getCatalogIdsArr($this->catalog);

        $productsEntity = new ProductCatalogEntity();
        $productsEntity->setMinPrice($dataShare->minPrice ?? null);
        $productsEntity->setMaxPrice($dataShare->maxPrice ?? null);
        $productsEntity->setTaxonomyGroups($dataShare->selectedTaxonomyGrouped ?? []);
        $products = $productsEntity->getProductsByCatalogId($catalogIds,$page);

        $countTaxonomy = $productsEntity->getTaxonomiesCount($catalogIds);
        $this->shareCountedTaxonomy($countTaxonomy);

        $priceRange = $productsEntity->getPricesRange($catalogIds);
        $dataShare->appendData('priceRange',$priceRange);

        $dataShare->appendData('countTaxonomy',$countTaxonomy);

        $collection = new PriceDTOCollection();
        foreach ($products as $item)
        {
            if($price = $item->prices->first())
            {
                $collection->append(new PriceDTO($price));
            }
        }

        $dataShare->appendData('priceProductItems',$collection->getItems());

        $productsTotal = $productsEntity->getCountProductsInCatalog($catalogIds);

        $dataShare->appendData('totalBlock',$productsEntity->getTotalBlockStr($productsTotal,$page));

        $dataShare->appendData('pagination',
            (new Pagination($productsTotal, $page,ProductCatalogEntity::LIMIT))
                ->setResourceId($this->catalog->id)
        );
    }

    protected function shareCountedTaxonomy(array $countTaxonomy)
    {
        $taxonomies = DataShare::getInstance()->taxonomies ?? null;
        $updatedTaxonomies = array();

        if(is_countable($taxonomies) && count($taxonomies))
        {
            foreach ($taxonomies as $group => $groupItems)
            {
                foreach ($groupItems as $taxonomyDto)
                {
                    if(empty($taxonomyDto->getChildren())) continue;
                    foreach ($taxonomyDto->getChildren()->getItems() as $subItem)
                    {
                        if($count = $countTaxonomy[$subItem->getId()] ?? null) {
                            $subItem->setCount($count);
                            $updatedTaxonomies[$group][$taxonomyDto->getUrl()][] = $subItem;
                        }
                    }
                }
            }
        }
        DataShare::getInstance()->appendData('taxonomy',$updatedTaxonomies);
    }

    protected function shareSortType()
    {
        $sortType = request()->get('sortType');
        if(!in_array($sortType,SortEnum::getEnums()))
        {
            $sortType = SortEnum::PRICE_ASC;
        }

        DataShare::getInstance()->appendData('sortType',$sortType);
    }

    /**
     * @return Collection
     */
    public function getCatalogs(){
        return DB::table(Catalog::table() . ' as model')->select([
            'model.id as id',
            'model.parent_id as parent_id',
            'model.key as key',
            'model.url as url',
            'model.photo as photo',
            'info.title as info_title',
            'info.url as info_url',
        ])->leftJoin(CatalogInfo::table() . ' as info', function ($join){
            $join->on('model.id','=','info.catalogs_id');
            $join->where('info.lang_key',Languages::getInstance()->getCurrentLocale());
        })
            ->where('model.active',true)->orderBy('model.order')->get();
    }

    function buildTree(Collection $dataset) {
        $tree = array();
        foreach ($dataset as $item) {

            $node = [
                'id' => $item->id,
                'parent_id' => $item->parent_id,
                'key' => $item->key,
                'url' => $item->url,
                'photo' => $item->photo,
                'info_title' => $item->info_title,
                'info_url' => $item->info_url,
            ];

            $id = $node['id'] ?? 0;

            if (!isset($tree[$id]['children'])) {
                $tree[$id]['children'] = array();
            }

            $node['children'] = $tree[$id]['children'];

            $tree[$id] = $node;

            $tree[$node['parent_id'] ?? 0]['children'][$node['id'] ?? 0] = &$tree[$id];
        }

        return $tree[0]['children'] ?? null;
    }


    protected function getChildrenNavDTOCollection(NavDTO $parent, array $children = []){
        $childrenCollection = new NavsCollection();

        if(is_countable($children)){
            foreach ($children as $item){
                $navDTO = (new NavDTO($item['info_title'],$parent->link .'/'. $item['url']))
                    ->setIcon($item['photo'] ? StorageAdapter::getInstance()->getStoragePath($item['photo']) : null);
                $navDTO->setChildren($this->getChildrenNavDTOCollection($navDTO,$item['children'] ?? []));
                $childrenCollection->append($navDTO);
            }
        }
        return $childrenCollection;
    }

    /**
     * @param Collection $items
     * @return NavsCollection
     */
    public function getCatalogNavCollection(Collection $items): NavsCollection
    {
        $collection = new NavsCollection();

        if(!is_countable($items) || !count($items)){
            return $collection;
        }

        $urlPrefix = (new Catalog())->getUrlPrefix();

        $treeArr = $this->buildTree($items);

        foreach ($treeArr as $item){
            $navDTO = (new NavDTO($item['info_title'],$urlPrefix . $item['url']))
                ->setIcon($item['photo'] ? StorageAdapter::getInstance()->getStoragePath($item['photo']) : null);
            $navDTO->setChildren($this->getChildrenNavDTOCollection($navDTO,$item['children'] ?? []));
            $collection->append($navDTO);
        }

        return $collection;
    }

    public function getCatalogTreeIds(int $catalogId)
    {
        return DB::select("SELECT  id,parent_id FROM
                        (SELECT * FROM catalogs ORDER BY parent_id, id) catalogs_sorted,
                        (SELECT @pv := '".$catalogId."') initialisation
                        WHERE FIND_IN_SET(parent_id, @pv) > 0
                        AND @pv := CONCAT(@pv, ',', id)");
    }

    public function getCatalogTreeIdsArr(int $catalogId): array
    {
        $idsArr = [$catalogId];
        try {
            $catalogs = $this->getCatalogTreeIds($catalogId);

            if(is_countable($catalogs) && count($catalogs)){
                foreach ($catalogs as $catalog){
                    $idsArr[] = $catalog->id;
                }
            }
        }catch (\Exception $exception){}
        return $idsArr;

    }
}
