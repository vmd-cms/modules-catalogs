<?php

namespace VmdCms\Modules\Catalogs\Entity;

use App\Modules\Products\DTO\ProductDTO;
use App\Modules\Products\Models\Product;
use App\Modules\Taxonomies\Models\Filter;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use VmdCms\Modules\Products\Collections\ProductDTOCollection;

class SearchEntity
{
    protected $needle;

    protected $limit;

    protected $withCatalogs;
    protected $withFilters;

    protected $catalogs;
    protected $filters;

    /**
     * SearchEntity constructor.
     * @param string|null $needle
     */
    public function __construct(string $needle = null)
    {
        $this->needle = $needle;
        $this->limit = 5;
        $this->withCatalogs = false;
        $this->withFilters = false;
    }

    /**
     * @param string $needle
     * @return $this
     */
    public function setNeedle(string $needle): SearchEntity
    {
        $this->needle = $needle;
        return $this;
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit(int $limit): SearchEntity
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @param bool $flag
     * @return $this
     */
    public function setWithCatalogs(bool $flag): SearchEntity
    {
        $this->withCatalogs = $flag;
        return $this;
    }

    /**
     * @param bool $flag
     * @return $this
     */
    public function setWithFilters(bool $flag): SearchEntity
    {
        $this->withFilters = $flag;
        return $this;
    }

    /**
     * @return ProductDTOCollection
     */
    public function getResults(): ProductDTOCollection
    {
        $results = new ProductDTOCollection();
        if(empty($this->needle)) return $results;

        $productIds = $this->getProductIds();

        if($this->withFilters){

        }

        $products = Product::whereIn('id',$productIds)->get();

        if(is_countable($products) && count($products)){
            foreach ($products as $product){
                $results->append(new ProductDTO($product));
            }
        }

        return $results;
    }

    /**
     * @param int $limit
     * @param int $page
     * @return array
     */
    protected function getProductIds(int $limit = 0, int $page = 1)
    {
        $productIdsArr = [];
        $query = DB::table('products')
            ->select('products.id')
            ->join('products_info','products_info.products_id','=','products.id')
            ->where('products.active','=',true)
            ->where(function ($q){
                $q->where('products_info.title','like','%' . $this->needle .'%')
                    ->orWhere('products_info.description_short','like','%' . $this->needle .'%');
            })
            ->orderBy('products.created_at','desc');
        if($page > 1 && $limit > 0) {
            $query->skip($limit * ($page - 1));
        }

        if($limit > 0){
            $query->limit($limit);
        }
        $products =  $query->get()->toArray();
        if(is_countable($products) && count($products)){
            foreach ($products as $item){
                if(isset($item->id)) $productIdsArr[] = $item->id;
            }
        }
        return $productIdsArr;
    }

    /**
     * @return array
     */
    public function getTaxonomyIdsByNeedle()
    {
        $idsArr = [];
        if(empty($this->needle)) return [];

        $results = DB::table('taxonomies as tax_parent')
            ->select('tax_parent.id')
            ->join('taxonomies','taxonomies.parent_id','=','tax_parent.id')
            ->join('prices_taxonomies','prices_taxonomies.taxonomies_id','=','taxonomies.id')
            ->join('prices','prices_taxonomies.prices_id','=','prices.id')
            ->join('products','prices.products_id','=','products.id')
            ->join('products_info','products_info.products_id','=','products.id')
            ->where('products.active','=',true)
            ->where(function ($q){
                $q->where('products_info.title','like','%' . $this->needle .'%')
                    ->orWhere('products_info.description_short','like','%' . $this->needle .'%');
            })
            ->groupBy('tax_parent.id')
            ->get();
        if(is_countable($results) && count($results)){
            foreach ($results as $item){
                if(isset($item->id)) $idsArr[] = $item->id;
            }
        }
        return $idsArr;
    }

    /**
     * @return array
     */
    public function getCatalogIdsByNeedle()
    {
        $idsArr = [];
        if(empty($this->needle)) return [];

        $query = DB::table('catalogs')
            ->select('catalogs.id')
            ->join('products_catalogs','products_catalogs.catalogs_id','=','catalogs.id')
            ->join('products','products_catalogs.products_id','=','products.id')
            ->where('products.active','=',true);

        $results = static::appendSearchCondition($query,$this->needle)->groupBy('catalogs.id')->get();

        if(is_countable($results) && count($results)){
            foreach ($results as $item){
                if(isset($item->id)) $idsArr[$item->id] = $item->id;
            }
        }
        return $idsArr;
    }

    public static function appendSearchCondition($query, $needle){
        return $query->join('products_info','products_info.products_id','=','products.id')->where(function ($q) use ($needle){
            $q->where('products_info.title','like','%' . $needle .'%')
                ->orWhere('products_info.description_short','like','%' . $needle .'%');
        });
    }
}
