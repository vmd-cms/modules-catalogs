<?php

namespace VmdCms\Modules\Catalogs\Entity;

use App\Modules\Catalogs\Entity\CatalogEntity;
use Illuminate\Database\Eloquent\Collection;

class CatalogNavs
{
    /**
     * @var \VmdCms\Modules\Catalogs\Entity\CatalogNavs
     */
    protected static $instance;

    /**
     * @var \VmdCms\Modules\Catalogs\Entity\CatalogEntity
     */
    protected $catalogEntity;

    /**
     * @var Collection
     */
    protected $catalog;

    /**
     * @var \Illuminate\Support\Collection
     */
    protected $catalogNavCollection;

    /**
     * @return \App\Modules\Catalogs\Entity\CatalogNavs
     */
    public static function getInstance()
    {
        if (!static::$instance) static::$instance = new static();
        return static::$instance;
    }

    protected function __construct(){

        $this->catalogEntity = new CatalogEntity();
        $this->catalog = $this->catalogEntity->getCatalogs();
        $this->catalogNavCollection = $this->catalogEntity->getCatalogNavCollection($this->catalog)->getItems();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getCatalog(): \Illuminate\Support\Collection
    {
        return $this->catalogNavCollection;
    }
}
