<?php

namespace VmdCms\Modules\Catalogs\Controllers;

use App\Modules\Catalogs\Entity\CatalogEntity;
use Illuminate\Http\Request;
use VmdCms\CoreCms\Controllers\CoreController;

class CatalogController extends CoreController
{
    public function catalogsPage()
    {

    }

    public function catalogItemPage(Request $request)
    {
        $url = filter_var($request->url,FILTER_SANITIZE_STRING);
        (new CatalogEntity())->catalogPageDataShare($url);

    }
}
