<?php

namespace VmdCms\Modules\Catalogs\Controllers;

use App\Modules\Catalogs\Entity\PageEntity;
use Illuminate\Support\Facades\Log;
use VmdCms\CoreCms\Controllers\CoreController;

class PageController extends CoreController
{
    public function catalogsPage()
    {
        try{
            (new PageEntity())->shareCatalogsPageData();
        }
        catch (\Exception $exception){
            Log::error($exception->getMessage());
            abort(404);
        }
        return view("content::layouts.pages.catalogs_page")->render();
    }

    public function brandsPage(){
        try{
            (new PageEntity())->shareBrandsPageData();
        }
        catch (\Exception $exception){
            Log::error($exception->getMessage());
            abort(404);
        }
        return view("content::layouts.pages.brands_page")->render();
    }
}
