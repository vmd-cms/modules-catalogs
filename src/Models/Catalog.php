<?php

namespace VmdCms\Modules\Catalogs\Models;

use Illuminate\Database\Eloquent\Builder;
use VmdCms\CoreCms\Contracts\Models\ActivableInterface;
use VmdCms\CoreCms\Contracts\Models\CloneableInterface;
use VmdCms\CoreCms\Contracts\Models\HasInfoInterface;
use VmdCms\CoreCms\Contracts\Models\HasMediaDimensionsInterface;
use VmdCms\CoreCms\Contracts\Models\OrderableInterface;
use VmdCms\CoreCms\Contracts\Models\SeoableInterface;
use VmdCms\CoreCms\Contracts\Models\TreeableInterface;
use VmdCms\CoreCms\CoreModules\Content\Models\Menu\Menu;
use VmdCms\CoreCms\Models\CmsModel;
use VmdCms\CoreCms\Traits\Models\Activable;
use VmdCms\CoreCms\Traits\Models\Cloneable;
use VmdCms\CoreCms\Traits\Models\HasImagePath;
use VmdCms\CoreCms\Traits\Models\HasInfo;
use VmdCms\CoreCms\Traits\Models\HasMediaDimensions;
use VmdCms\CoreCms\Traits\Models\HasUrlPath;
use VmdCms\CoreCms\Traits\Models\JsonData;
use VmdCms\CoreCms\Traits\Models\Orderable;
use VmdCms\CoreCms\Traits\Models\Seoable;
use VmdCms\CoreCms\Traits\Models\Treeable;
use VmdCms\CoreCms\Traits\Related\HasCrossRelatedService;
use VmdCms\Modules\Taxonomies\Traits\Filters;

class Catalog extends CmsModel implements ActivableInterface, TreeableInterface, HasInfoInterface, OrderableInterface, SeoableInterface, CloneableInterface, HasMediaDimensionsInterface
{
    use Activable, Treeable, HasInfo, HasUrlPath, HasImagePath, Orderable, HasCrossRelatedService, Filters, JsonData, Seoable, Cloneable, HasMediaDimensions;

    public static function table(): string
    {
        return 'catalogs';
    }

    protected function getTaxonomyCrossModel(): string
    {
        return CatalogTaxonomy::class;
    }

    /**
     * @return string
     */
    protected function getConfigModuleKey()
    {
        return 'catalogs_module.prefix_single';
    }

    public static function getModelKey(){
        return null;
    }

    protected static function boot()
    {
        parent::boot();

        if(static::getModelKey()){
            static::addGlobalScope('key', function (Builder $builder) {
                $builder->where('key', static::getModelKey());
            });
            static::creating(function (Catalog $model){
                if(!empty($model->order)) return;
                $lastItem = Catalog::where('key',static::getModelKey())->orderBy('order','desc')->first();
                if($lastItem instanceof Catalog){
                    $model->order = intval($lastItem->order) + 1;
                }
            });
            static::saving(function (Catalog $model){
                $model->key = static::getModelKey();
            });
        }
    }

    private function getInfoClass() : string
    {
        return CatalogInfo::class;
    }
}
