<?php

namespace VmdCms\Modules\Catalogs\Models;

use VmdCms\CoreCms\Models\CmsInfoModel;

class CatalogInfo extends CmsInfoModel
{
    public static function table(): string
    {
        return 'catalogs_info';
    }
}
