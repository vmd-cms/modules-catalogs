<?php

namespace VmdCms\Modules\Catalogs\Models;

use VmdCms\CoreCms\Exceptions\CoreException;
use VmdCms\CoreCms\Exceptions\Modules\ModuleNotActiveException;
use VmdCms\CoreCms\Models\CmsCrossModel;

class PageCatalog extends CmsCrossModel
{
    public static function table(): string
    {
        return 'pages_catalogs';
    }

    public function getBaseModelClass(): string
    {
        if(!class_exists(\VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::class)){
            throw new ModuleNotActiveException();
        }

        return \VmdCms\CoreCms\CoreModules\Content\Models\Pages\Page::class;
    }

    public function getTargetModelClass(): string
    {
        return Catalog::class;
    }
}
