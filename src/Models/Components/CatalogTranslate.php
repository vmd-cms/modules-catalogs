<?php

namespace VmdCms\Modules\Catalogs\Models\Components;

use VmdCms\CoreCms\CoreModules\Translates\Models\Translate;

class CatalogTranslate extends Translate
{
    public static function getModelGroup(): ?string
    {
        return 'catalog';
    }
}
