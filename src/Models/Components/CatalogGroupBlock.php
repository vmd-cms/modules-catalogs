<?php

namespace VmdCms\Modules\Catalogs\Models\Components;

use VmdCms\CoreCms\CoreModules\Content\Models\Blocks\Block;

class CatalogGroupBlock extends Block
{
    public static function getModelGroup(): ?string
    {
        return 'catalog';
    }
}
