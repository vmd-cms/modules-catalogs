<?php

namespace VmdCms\Modules\Catalogs\Models\Components\Blocks;

use VmdCms\Modules\Catalogs\Models\Components\CatalogGroupBlock;
use VmdCms\Modules\Catalogs\Services\BlockEnum;

class CatalogInfoBlock extends CatalogGroupBlock
{
    public static function getModelKey(): ?string
    {
        return BlockEnum::CATALOG_INFO_BLOCK;
    }

    public function getJsonDataFieldsArr() : array
    {
        return ['title'];
    }
}
