<?php

namespace VmdCms\Modules\Catalogs\Models;

use VmdCms\CoreCms\Models\CmsCrossModel;
use VmdCms\Modules\Taxonomies\Models\Taxonomy;

class CatalogTaxonomy extends CmsCrossModel
{
    public static function table(): string
    {
        return 'catalogs_taxonomies';
    }

    public function getBaseModelClass(): string
    {
        return Catalog::class;
    }

    public function getTargetModelClass(): string
    {
        return Taxonomy::class;
    }
}
