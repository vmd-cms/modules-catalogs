<?php

return [
    /**
     * fields
     **/
    'parent_catalog' => 'Родительский Каталог',
    'filters' => 'Фильтры',

    /**
     * pages
     */
    'catalog' => 'Каталог',
    'catalogs' => 'Каталоги',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
