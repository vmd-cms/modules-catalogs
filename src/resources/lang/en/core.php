<?php

return [
    /**
     * fields
     **/
    'parent_catalog' => 'Parent Catalog',
    'filters' => 'Filters',

    /**
     * pages
     */
    'catalog' => 'Catalog',
    'catalogs' => 'Catalogs',

    /**
     * messages
     **/

    /**
     * buttons
     **/

    /**
     * validates
     */
];
