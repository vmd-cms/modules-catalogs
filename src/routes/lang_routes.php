<?php
use Illuminate\Support\Facades\Route;
use VmdCms\CoreCms\CoreModules\Content\Middleware\DataShareMiddleware;

return function (){
    Route::post('/catalog/products/load-more', 'App\Modules\Catalogs\Controllers\CatalogController@loadMoreProducts');
    Route::get('/catalogs', 'App\Modules\Catalogs\Controllers\CatalogController@catalogsPage');
    Route::get('/catalog/{url}', 'App\Modules\Catalogs\Controllers\CatalogController@catalogBasePage');
    Route::get('/catalog/{base}/{url}', 'App\Modules\Catalogs\Controllers\CatalogController@catalogItemPage')->where('url','.*');
};
