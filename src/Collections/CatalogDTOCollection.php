<?php

namespace VmdCms\Modules\Catalogs\Collections;

use VmdCms\CoreCms\Collections\CoreCollectionAbstract;
use VmdCms\Modules\Catalogs\Contracts\CatalogDTOCollectionInterface;
use VmdCms\Modules\Catalogs\Contracts\CatalogDTOInterface;

class CatalogDTOCollection extends CoreCollectionAbstract implements CatalogDTOCollectionInterface
{
    /**
     * @param CatalogDTOInterface $catalogEntity
     */
    public function append(CatalogDTOInterface $catalogEntity)
    {
        $this->collection->add($catalogEntity);
    }
}
